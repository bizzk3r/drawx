//
//  SwiftRobotControlCenter.swift
//  MyRobot
//
//  Created by Ivan Vasilevich on 10/4/14.
//  Copyright (c) 2014 Ivan Besarab. All rights reserved.
//

import UIKit
//all robot commands can be founded in GameViewController.h
class SwiftRobotControlCenter: RobotControlCenter {
    
    //in this function change levelName
    override func viewDidLoad() {
        levelName = "L4H" // level name
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // drawX
        
        drawX()
        
        
    }
    
    func drawX(){
        put()
        while rightIsClear {
            makeDiag()
        }
        turnAroundAndMoveToLeftCorner()
        turnRight()
        
        while frontIsClear {
            makeDiag()
        }
    }
    
    func turnAroundAndMoveToLeftCorner(){
        turnAround()
        while frontIsClear {
            move()
        }
        put()
    }
    
    func makeDiag() {
        move()
        turnRight()
        move()
        turnLeft()
        put()
        
    }
    func turnAround(){
        turnRight()
        turnRight()
    }
    
    func turnLeft (){
        turnRight()
        turnRight()
        turnRight()
    }
    
}


